package vlado.sampleCodes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Vladimír Leško (DZCD9I9)
 */
@SpringBootApplication
@ComponentScan(basePackages = "vlado.sampleCodes")
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
public class EntryPoint {
	public static void main (String... args) {
		SpringApplication.run(EntryPoint.class, args);
		System.out.println("HEllo worlds");
	}
}
